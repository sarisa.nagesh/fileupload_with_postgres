/* now lets create all tables */


CREATE TABLE "public".user_info
(
    user_id SERIAL,
    user_name character varying(50) NOT NULL,
    mobile_num numeric(12,0) NOT NULL,
    email_id character varying,
    password character varying,
    image_path varchar(100) NULL,
    CONSTRAINT user_pkey PRIMARY KEY (user_id),
    CONSTRAINT user_email_key UNIQUE (email_id),
    CONSTRAINT user_user_name_key UNIQUE (user_name),
    CONSTRAINT user_mobile_num_key UNIQUE (mobile_num)
);

