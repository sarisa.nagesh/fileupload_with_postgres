const pool = require('../config/db');


function updateImagePath(path, user_id) {
    
    const query = {
        name: 'insert image path in db',
        text: `UPDATE  "public".user_info SET  image_path=$2 WHERE user_id=$1
          `,
        values: [
            user_id,
            path,
        ]
    }
    try {
        return pool.query(query);

      
    } catch (error) {
        console.log(error);

        return error;
    }
}

async function getUser (mobile_num) {
    const query = {
        name: 'Check user exists',
        text: 'SELECT * FROM "public".user_info WHERE mobile_num = $1',
        values: [ mobile_num ],
    };

    console.log(`select query called! ${mobile_num}`);

    try {
        return await pool.query(query);

        // console.log(`query result is ${result}`);

        // return result.rows.length;
    } catch (error) {
        console.log(error);

        return error;
    }
}

async function insertUser (userDetails) {
    const query = {
        name: 'insert user in db',
        text: `INSERT INTO "public".user_info (user_name, mobile_num, password, email_id) 
                VALUES($1, $2, $3, $4) RETURNING *`,
        values: [
            userDetails.user_name,
            userDetails.mobile_num,
            userDetails.encryptedPassword,
            userDetails.email_id,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

module.exports = { updateImagePath, getUser, insertUser };
