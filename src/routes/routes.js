const express = require("express");
const router = express.Router();

const userRegister = require("../controllers/userRegistration");
const userLogin = require("../controllers/userLogin");

const Router = require("../controllers/file");

router.post("/userRegister", userRegister);
router.post("/userLogin", userLogin);

router.post("/UploadFile", Router);

module.exports = router;
