const path = require("path");
const express = require("express");
var fs = require("fs");
const multer = require("multer");
const Router = express.Router();
const DIR = "./images";
const adduserService = require("../services/userServices");
const parseIp = require("../middleware/parseIp");

if (!fs.existsSync(DIR)) {
  fs.mkdirSync(DIR);
}

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    let fieldname = file.fieldname.split("|");
    let file_path = DIR + "/" + fieldname[0];
    console.log(file_path);

    if (!fs.existsSync(file_path)) {
      fs.mkdirSync(file_path);
      cb(null, file_path);
    } else {
      cb(null, file_path);
    }
  },
  filename: (req, file, cb) => {
    console.log("file.fieldname====>", file.fieldname);
    cb(null, "wisdomdoc" + "-" + Date.now() + file.originalname);
  },
});

let Upload = multer({ storage: storage });

Router.post("/UploadFile", Upload.any(), UploadFile);

async function UploadFile(req, res, next) {
  var response = req.files;

  if (!req.files[0].fieldname) {
    console.log("No file received");
    return res.send({
      success: false,
    });
  } else {
    console.log("response", response[0]);
    try {
      const { path, mimetype } = response[0];
      adduserService.updateImagePath(path, req.body.user_id);
      res.send({
        message: "file uploaded successfully.",
        ipAddress: parseIp(req),
      });
    } catch (error) {
      res.status(400).send({
        message: "Error while uploading file. Try again later.",
        ipAddress: parseIp(req),
      });
    }
  }
}

module.exports = Router;
